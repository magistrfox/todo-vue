import Vue from "vue";
import App from "./App.vue";
import store from "./store";
import { BootstrapVue } from "bootstrap-vue";
import router from "./router";
import Vuelidate from 'vuelidate'
import Parse from 'parse'


Vue.use(Parse);
Vue.use(BootstrapVue);
Vue.use(Vuelidate);
Vue.config.productionTip = false;

Parse.initialize('MO1VhwWL7Y4dQ7NvUltiRZbfhkC7k9EDfWakBF6d', 'Qg3uCi72Ig9B7spmci2nM9OcUKH4YCohDaPYNl7U');
Parse.serverURL = 'https://parseapi.back4app.com/';

new Vue({
  store,
  router,
  render: h => h(App)
}).$mount("#app");
