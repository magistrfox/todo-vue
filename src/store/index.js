import Vue from "vue";
import Vuex from "vuex";

Vue.use(Vuex);

export default new Vuex.Store({
  state: {

  },
  mutations: {
    addUser(state, id) {
      state[`${id}`] = {
        id: id
      }
    }
  },
  actions: {},
  modules: {}
});
