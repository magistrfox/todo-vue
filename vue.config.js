module.exports = {
  publicPath: process.env.NODE_ENV === 'production'
    ? '/' + process.env.CI_PROJECT_NAME + '/'
    : '/',
  css: {
    loaderOptions: {
      scss: {
        prependData: `
        @import "@/bootstrap_custom/_overrides.scss";
        @import "node_modules/bootstrap/scss/bootstrap";
        @import "node_modules/bootstrap-vue/src/index.scss";
        `
      }
    }
  }
};
